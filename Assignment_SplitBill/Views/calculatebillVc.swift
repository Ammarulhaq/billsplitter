//
//  calculatebillVc.swift
//  Assignment_SplitBill
//
//  Created by Ammar on 05/07/2018.
//  Copyright © 2018 Ammar. All rights reserved.
//

import UIKit

class calculatebillVc: UIViewController,UITextFieldDelegate,UIPickerViewDataSource, UIPickerViewDelegate {
    
    
    @IBOutlet weak var txt_noofperson: UITextField!
    @IBOutlet weak var slider_tip: UISlider!
    @IBOutlet weak var txt_adjusttip: UITextField!
    @IBOutlet weak var lbl_value: UILabel!
    var price : Int = 0
    
    @IBOutlet weak var lbl_Result: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        lbl_value.text = "10 %"
        txt_adjusttip.delegate = self
        setupPickerView()
        
    }
    
    @IBAction func silderAction(_ sender: UISlider) {
       
        lbl_value.text = "\(Int(sender.value)) %"
        txt_adjusttip.text = "\(Int(sender.value))"
        
        if !(txt_noofperson.text?.elementsEqual(""))!
        {
            
            let temp = sender.value / 100
            let percentValue = Float(price) * temp
            let result = price - Int(percentValue)
            
            if let stringValue = txt_noofperson.text{
                if let intValue = Int(stringValue){
                     let PricePerHead = result / intValue
                    lbl_Result.text = "Price to pay per head = \(PricePerHead)"
                }
            }
    }
    }
    
    //UITextField Delegates
    func textFieldDidBeginEditing(_ textField: UITextField) {
        print("TextField did begin editing method called")
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        print("TextField should begin editing method called")
        return true;
    }
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        print("TextField should clear method called")
        return true;
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        
        return true;
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        return true;
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        print("TextField should return method called")
        textField.resignFirstResponder();
        return true;
    }
    
    func setupPickerView()
    {
        let thePicker = UIPickerView()
        thePicker.delegate = self
        txt_noofperson.inputView = thePicker
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 0/255, green: 0/255, blue: 139/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(calculatebillVc.doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(calculatebillVc.cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        txt_noofperson.inputAccessoryView = toolBar
    }
    
    @objc func doneClick() {
        txt_noofperson.resignFirstResponder()
    }
    @objc func cancelClick() {
        txt_noofperson.resignFirstResponder()
    }
    
    
    // MARK: UIPickerView Delegation
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        return 10
    }
    
    func pickerView( _ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        return "\(row)"
    }
    
    func pickerView(_ pickerView: UIPickerView!, didSelectRow row: Int, inComponent component: Int) {
        
        
        txt_noofperson.text = "\(row)"
    }
    
    
    @IBAction func btnTapped_Adjusttip(_ sender: Any) {
        
        lbl_value.text = "\(txt_adjusttip.text ?? "10") %"
        if let stringValue = txt_adjusttip.text{
            if let intValue = Int(stringValue){
                slider_tip.setValue(Float(intValue), animated: true)
            }
        }
        
        if !(txt_noofperson.text?.elementsEqual(""))!
        {
            
            let temp = slider_tip.value / 100
            let percentValue = Float(price) * temp
            let result = price - Int(percentValue)
            
            if let stringValue = txt_noofperson.text{
                if let intValue = Int(stringValue){
                    let PricePerHead = result / intValue
                    lbl_Result.text = "Price to pay per head = \(PricePerHead)"
                }
            }
        }
        
    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
