//
//  getAmountVc.swift
//  Assignment_SplitBill
//
//  Created by Ammar on 05/07/2018.
//  Copyright © 2018 Ammar. All rights reserved.
//

import UIKit

class getAmountVc: UIViewController {
    @IBOutlet weak var txt_amount: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        txt_amount.becomeFirstResponder()
    }
    
    @IBAction func btnTapped_Continue(_ sender: Any) {
        
        if (txt_amount.text?.elementsEqual(""))!
        {
            let alert = UIAlertController(title: "Sorry", message: "Kindly enter the amount to continue", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
            self.present(alert, animated: true)
        }
        else
        {
            
            self.performSegue(withIdentifier: "next", sender: sender)
            
            
        }
        
    }
    
    
    //Hiding status bar
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "next") {
            let secondViewController = segue.destination as! calculatebillVc
            secondViewController.price = Int(txt_amount.text!)!
        }
    }
}

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */


